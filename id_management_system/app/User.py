import sqlalchemy
from app import Base, session
from marshmallow import fields, validate, Schema, post_load
from dotenv import load_dotenv
from flask import Flask
from flask_restful import Api
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy
import os
from flask_jwt_extended import JWTManager

class User(Base):
    __tablename__ = "users"
    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key = True)
    first_name = sqlalchemy.Column(sqlalchemy.String(100), nullable = False)
    last_name = sqlalchemy.Column(sqlalchemy.String(100), nullable = False)
    username = sqlalchemy.Column(sqlalchemy.String(100), unique = True, nullable = False)
    password = sqlalchemy.Column(sqlalchemy.String(100), nullable = False)
    phone_number = sqlalchemy.Column(sqlalchemy.String(100), unique = True, nullable = False)
    email = sqlalchemy.Column(sqlalchemy.String(100), unique = True, nullable = False)
    is_active = sqlalchemy.Column(sqlalchemy.Boolean, nullable = False)
    is_admin = sqlalchemy.Column(sqlalchemy.Boolean, nullable = False)

    def __init__(self,first_name, last_name, username, password, phone_number, email, is_active, is_admin):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.phone_number = phone_number
        self.email = email
        self.is_active = is_active
        self.is_admin = is_admin

    def __repr__(self):
        return '<User %s>' % self.first_name

    

class UserSchema(Schema):
        first_name = fields.Str(required=True)
        last_name  = fields.Str(required=True)
        username  = fields.Str(required=True)
        password = fields.Str(required=True)
        phone_number = fields.Str(required=True, validate=[validate.Regexp('^[0-9]{10}$')], error="invalid number")
        email = fields.Email(required=True)
        is_active = fields.Bool(required=True)
        is_admin = fields.Bool(required=True)

        class Meta:
            model = User
        @post_load
        def makeUser(self,data,**kwargs):
            return User(**data)

user_schema = UserSchema()
users_schema = UserSchema(many = True)
