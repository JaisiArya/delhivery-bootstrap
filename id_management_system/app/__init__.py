from flask import Flask
from flask_restful import Api
from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy
import os
from dotenv import load_dotenv
from flask_jwt_extended import JWTManager
from flask_restful import Resource
import json
from kafka import KafkaProducer

app = Flask(__name__)
load_dotenv()
app.secret_key = os.getenv("SECRET_KEY").encode('utf-8')
db = sqlalchemy.create_engine(os.getenv("POSTGRES_DATABASE_URI"))
Base = declarative_base()
Session = sqlalchemy.orm.sessionmaker(bind=db)
session = Session()
api = Api(app)
app.config['JWT_SECRET_KEY'] = os.getenv("SECRET_KEY").encode('utf-8')
jwt = JWTManager(app)

producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))


from app.api import CreateUser, UserListResource, UserResource, QueryUsers, Login , protected

