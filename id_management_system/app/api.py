from app.User import user_schema, users_schema, User
from app import api, session, jwt , producer
from flask import request, jsonify
from flask_restful import Resource
from marshmallow import ValidationError
from flask_jwt_extended import (
    create_access_token, decode_token, get_jwt_identity,
    jwt_required, verify_jwt_in_request, get_jwt_claims
    )
from sqlalchemy import text
import hashlib
from functools import wraps
import pickle
import redis
from http import HTTPStatus
from sqlalchemy.exc import IntegrityError
import logging


def checkadmin(function):
    @wraps(function)
    @jwt_required
    def wrapper(*args, **kwargs):
        current_user = get_jwt_identity()
        if current_user["is_admin"]:
            return function(*args, **kwargs)
        else:
            return {"e": "Action not allowed"}, HTTPStatus.FORBIDDEN
    return wrapper

def kafka_msg(user, action):
    return {
        "User": users_schema,
        "action": action,

    }

logging.basicConfig(filename="details.log", level = logging.INFO ,format='%(asctime)s %(message)s') 
logger=logging.getLogger(__name__)


class CreateUser(Resource):
    @checkadmin
    def post(self):
        user = request.get_json()
        try:
            user["password"] = hashlib.sha256(user["password"].encode("utf-8")).hexdigest()
            new_user = user_schema.load(user)
            session.add(new_user)
            session.commit()
            producer.send('ims',kafka_msg(new_user,"CREATE"))
            logger.info("User created")
            return {"mesage": "Created"}, HTTPStatus.CREATED
        except ValidationError as err:
            logging.exception(str(err.messages))
            return {"e":err.messages}, HTTPStatus.BAD_REQUEST
        except IntegrityError as err:
            logging.exception(str(err.messages))
            session.rollback()
            return {"e": str(err.__cause__)}, HTTPStatus.INTERNAL_SERVER_ERROR
api.add_resource(CreateUser, '/users/create')


class UserListResource(Resource):
    @checkadmin
    def get(self):
        users = session.query(User).all()
        return users_schema.dump(users)
api.add_resource(UserListResource, '/users')


redis_conn = redis.StrictRedis(host='localhost', port=6379, db=0)

class UserResource(Resource):
    def get(self, user_id):
        
        try:
            user = redis_conn.get(name = str(user_id))
            if user is None or len(user) <= 1:
                user = session.query(User).get(user_id)
                if user :
                    user = user_schema.dump(user)
                    user = pickle.dumps(user)
                    redis_conn.set(name= "user::id: {}".format(str(user_id)), value= user, ex=3000)
                else:
                    return {"message": "user doesnot exist"}, HTTPStatus.BAD_REQUEST
            return pickle.loads(user)
        except redis.RedisError:
            return {"e": "redis connection error"}, HTTPStatus.INTERNAL_SERVER_ERROR

    @checkadmin
    def patch(self, user_id):
        data = request.get_json()
        for att in data:
            if att == "password":
                data[att] = hashlib.sha256(data[att].encode("utf-8")).hexdigest()
        user = redis_conn.get(name = str(user_id))
        if user:
            redis_conn.delete(user_id)
        userQuery = session.query(User).filter(User.id == user_id)
        user = userQuery.first()
        try:

            user = session.query(User).filter(User.id == user_id).update(data)            
            session.commit()
            producer.send('ims',kafka_msg(user,"UPDATE"))
            logger.info("User updated")
            return {"message": "User updated successfully"}, HTTPStatus.OK
        except ValidationError as err:
            return {"e":err.messages}, HTTPStatus.BAD_REQUEST
        except IntegrityError as err:
            session.rollback()
            return {"e": str(err.__cause__)}, HTTPStatus.INTERNAL_SERVER_ERROR
        except redis.RedisError:
            return {"e": "redis connection error"}, HTTPStatus.INTERNAL_SERVER_ERROR
        return user_schema.dump(data)
        
 
    def delete(self, user_id):
        user = session.query(User).get(user_id)
        if user:
            session.delete(user)
            session.commit()
            redis_conn.delete(user_id)
            producer.send('ims',kafka_msg(user,"DELETE"))
            logger.info("User deleted")
            return {"message": "Deleted"}, HTTPStatus.OK
        else:
            return {"e": "user doesnot exist"}, HTTPStatus.NOT_FOUND
api.add_resource(UserResource, '/users/<int:user_id>')


class QueryUsers(Resource):
    def get(self):
        Query = request.args
        print(Query)
        for att in Query:
            if att not in [column.att for column in User.__table__.columns]:
                return {"e":att + " query not exist"}, HTTPStatus.NOT_FOUND
        result = session.query(User).filter_by(**args).all()
        return users_schema.dump(result), HTTPStatus.OK
api.add_resource(QueryUsers,'/users/<int:userId>')


class Login(Resource):
    def post(self):
        data = request.get_json()
        if not data:
            return {"msg": "Missing data in request"}, HTTPStatus.BAD_REQUEST
            
        user = session.query(User).filter(User.username == data["username"]).first()
        if user and not user.is_active:
            return {"e": "inactive user"}, HTTPStatus.BAD_REQUEST
        if user and user.password == hashlib.sha256(data["password"].encode("utf-8")).hexdigest():
            identity = {
                "id": user.id,
                "is_admin": user.is_admin,
                "is_active": user.is_active
            }
            jwt_accessToken = create_access_token(identity=identity , fresh=True)
            return {"jwt": jwt_accessToken}, HTTPStatus.OK
api.add_resource(Login,'/users/login')

class jwtverify(Resource):
    def post(self):
        data = request.get_json()
        jwtdecoded = decode_token(data["jwt"])

        return jwtdecoded, HTTPStatus.OK
api.add_resource(jwtverify, '/users/jwtverify')


class protected(Resource):
    @jwt_required
    def get(self):
        current_user = get_jwt_identity()
        user = session.query(User).filter_by(id= current_user).all()
        user1= user[0].is_admin
        return {"is admin": user1}, HTTPStatus.OK
api.add_resource(protected, '/users/protected') 